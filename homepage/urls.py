from django.contrib import admin

from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.home_fix, name='home_fix'),
    path('about/', views.about, name='about'),
    path('works/', views.my_work, name='my_work'),
    path('contact/', views.contact_me, name='contact_me'),
    path('edufia/', views.work_edufia, name='work_edufia'),
    path('pemira/', views.work_edufia, name='work_pemira'),
    path('make-schedule/', views.forms, name='forms'),
    path('edit-schedule/', views.seeSchedule, name='seeSchedule'),
    path('del_all/', views.seeSchedule, name='del_all'),
    path('sched_delete/<int:pk>', views.seeSchedule, name='sched_delete'),
    
]
