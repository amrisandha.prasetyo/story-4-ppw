from django.db import models
from django.utils import timezone
from datetime import datetime

# Create your models here.




class Schedule(models.Model):
    CATEGORY = [('Exam','Exam'), ('Quiz','Quiz'), ('Homework','Homework'), ('Study','Study'), ('Meeting','Meeting'), ('Event','Event')]
    dates = models.DateField()
    time = models.TimeField()
    activity = models.CharField(max_length=50)
    location = models.CharField(max_length=50)
    category = models.CharField(max_length=50, choices=CATEGORY)

def __str__(self):
    return self.activity

