from django.shortcuts import render
from django import forms
from django.forms import ModelForm
from .models import ScheduleFormModel

# Create your views here.
def home_fix(request):
    return render(request, 'home_fix.html')

def about(request):
    return render(request, 'about.html')

def contact_me(request):
    return render(request, 'contact_me.html')

def my_work(request):
    return render(request, 'my_work.html')

def work_edufia(request):
    return render(request, 'work_edufia.html')

def work_pemira(request):
    return render(request, 'work_pemira.html')

def forms(request):
    if request.method == 'POST':
        form = ScheduleForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('../forms')
    else:
        form = ScheduleForm()

    content = {'title' : 'Form Schedule',
                'form' : form}

    return render(request, 'forms.html', content)

def seeSchedule(request):
    data = Schedule.objects.all()
    content = {'title' : 'Schedule',
                'data' : data}
    return render(request, 'seeSchedule.html', content)

def del_all(request):
    if request.method == "POST":
        Schedule.objects.all().delete()
        data = Schedule.objects.all()
        content = {'title' : 'Schedule',
                'data' : data}
        return HttpResponseRedirect('/seeSchedule')
    return HttpResponseRedirect('/seeSchedule')

def sched_delete(request, pk):
    Schedule.objects.filter(pk=pk).delete()
    data = Schedule.objects.all()
    content = {'title' : 'Schedule',
                'data' : data}
    return HttpResponseRedirect('/seeSchedule')
